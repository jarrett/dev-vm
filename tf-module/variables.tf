variable "id" {
  description = "name of enviornment - used for hostname and fqdn prefix"
}

variable "username" {
  description = "name of vps user"
}

variable "image" {
  description = "image definition"
  type        = map(string)
  default = {
    publisher = "SUSE"
    offer     = "opensuse-leap-15-4"
    sku       = "gen2"
    version   = "latest"
  }
}

variable "vm_size" {
  description = "Virtual Machine Size"
  default     = "Standard_D2as_v4"
}

variable "rsa_pubkey_path" {
  description = "path to rsa pub key for ssh"
  default     = "~/.ssh/id_rsa.pub"
}

variable "location" {
  description = "Azure Region"
}
