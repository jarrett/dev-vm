provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "main" {
  name     = "${var.id}-rg"
  location = var.location
}

resource "azurerm_linux_virtual_machine" "main" {
  name                       = var.id
  location                   = azurerm_resource_group.main.location
  resource_group_name        = azurerm_resource_group.main.name
  network_interface_ids      = [azurerm_network_interface.main.id]
  size                       = var.vm_size
  admin_username             = var.username
  encryption_at_host_enabled = true

  source_image_reference {
    publisher = var.image.publisher
    offer     = var.image.offer
    sku       = var.image.sku
    version   = var.image.version
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("${var.rsa_pubkey_path}")
  }
}
